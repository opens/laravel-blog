@extends('layouts.app')

@section('content')
    <h1>Edit</h1>

    <form method="POST" action="{{ route('blog.update', $post->id) }}">
        @csrf
        @method('PUT')

        @include('posts.form')

        <button type="submit">Save</button>
    </form>

@endsection