@extends('layouts.app')

@section('content')
    <a class="text-center" href="{{ route('blog.edit', $post->id) }}"><h1>{{ $post->title }}</h1></a>
    <p>{!! $post->html !!}</p>
@endsection