@extends('layouts.app')

@section('content')
    <h1 class="text-center">Posts</h1>

    @foreach ($posts as $post)
        <div>
            <a href="{{ route('blog.show', $post->id) }}"><h3>{{ $post->title }}</h3></a>
            {{$post->created_at}}
            <p>{!! $post->html !!}</p>
        </div>
        <hr>

    @endforeach
@endsection