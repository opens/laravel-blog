<div class="mb-3">
    <input class="w-100" placeholder="Title" type="text" name="title" id="title" value="{{ $post->title ?? '' }}">
</div>
<div>
    <textarea name="html" id="html" rows="30">{{ $post->html ?? '' }}</textarea>
</div>
<script>
    tinymce.init({
        selector: '#html'
    });
</script>