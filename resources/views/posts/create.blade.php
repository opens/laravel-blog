@extends('layouts.app')

@section('content')
    <h1>Create</h1>

    <form method="POST" action="{{ route('blog.store') }}">
        @csrf

        @include('posts.form')

        <button type="submit">Create</button>
    </form>
@endsection