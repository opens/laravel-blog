<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Http\Resources\PostResources;
use App\Models\Post;

class PostsController extends Controller
{
    public function index()
    {
        $posts = PostResources::collection(Post::all());
        return view('posts.index', compact('posts'));
    }

    public function create()
    {
        return view('posts.create');
    }

    public function show($id)
    {
        $post = new PostResources(Post::findOrFail($id));
        return view('posts.show', compact('post'));
    }

    public function edit($id)
    {
        $post = new PostResources(Post::findOrFail($id));
        return view('posts.edit', compact('post'));
    }

    public function store(PostRequest $request)
    {
        $validatedData = $request->validated();

        $post = Post::create($validatedData);

        return redirect()->route('blog.index');

    }

    public function update(PostRequest $request, $id)
    {
        $validatedData = $request->validated();

        $post = Post::findOrFail($id);
        $post->update($validatedData);

        return redirect()->route('blog.index');
    }


    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();

        return redirect()->route('blog.index');
    }
}
