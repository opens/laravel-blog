<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            [
                'id' => 1,
                'title' => 'Nisi sit consectetur nulla habitasse dic',
                'html' => 'Nisi sit consectetur nulla habitasse dictum lectus quam, molestie sit sed morbi ornare pellentesque risus eleifend dictumst. Interdum in risus faucibus. Pellentesque accumsan lacinia non vestibulum pulvinar non odio. Interdum mauris lorem sit ornare nisi molestie non lorem faucibus. Arcu sit nisi morbi faucibus. Platea molestie ornare pulvinar tempus vestibulum vitae libero, quis, vulputate mauris non non urna pulvinar justo luctus habitasse luctus dictum. Et cras velit venenatis ornare quam, et',
                'created_at' => '2023-05-17 22:21:53',
            ],
            [
                'id' => 2,
                'title' => 'Platea molestie ornare pulv',
                'html' => 'Nisi sit consectetur nulla habitasse dictum lectus quam, molestie sit sed morbi ornare pellentesque risus eleifend dictumst. Interdum in risus faucibus. Pellentesque accumsan lacinia non vestibulum pulvinar non odio. Interdum mauris lorem sit ornare nisi molestie non lorem faucibus. Arcu sit nisi morbi faucibus. Platea molestie ornare pulvinar tempus vestibulum vitae libero, quis, vulputate mauris non non urna pulvinar justo luctus habitasse luctus dictum. Et cras velit venenatis ornare quam, etNisi sit consectetur nulla habitasse dictum lectus quam, molestie sit sed morbi ornare pellentesque risus eleifend dictumst. Interdum in risus faucibus. Pellentesque accumsan lacinia non vestibulum pulvinar non odio. Interdum mauris lorem sit ornare nisi molestie non lorem faucibus. Arcu sit nisi morbi faucibus. Platea molestie ornare pulvinar tempus vestibulum vitae libero, quis, vulputate mauris non non urna pulvinar justo luctus habitasse luctus dictum. Et cras velit venenatis ornare quam, et',
                'created_at' => '2023-05-17 22:21:53',
            ],
        ]);
    }
}